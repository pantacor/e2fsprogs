LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

PV_REMOVE_FILES := badblocks \
	blkid \
	dumpe2fs \
	e2freefrag \
	e2fsck \
	e2label \
	e2mmpstatus \
	e2scrub \
	e2scrub_all \
	e2undo \
	e4crypt \
	filefrag \
	findfs \
	logsave \
	mklost+found \
	tune2fs

LOCAL_HOST_MODULE := e2fsprogs
LOCAL_DESCRIPTION := e2fsprogs for image building
LOCAL_COPY_FILES := contrib/populate-extfs.sh:bin/

include $(BUILD_AUTOTOOLS)

include $(CLEAR_VARS)

LOCAL_MODULE := e2fsprogs
LOCAL_DESCRIPTION := e2fsprogs for image building

LOCAL_AUTOTOOLS_VERSION := 2.0.4
LOCAL_AUTOTOOLS_CONFIGURE_ARGS := \
	--disable-debugfs \
	--disable-imager \
	--disable-defrag \
	--disable-e2initrd-helper \
	--disable-uuidd \
	--disable-fuse2fs \
	$(NULL)

LOCAL_AUTOTOOLS_CONFIGURE_ENV := \
	$(NULL)

define LOCAL_AUTOTOOLS_CMD_POST_INSTALL
	$(Q) for f in ${PV_REMOVE_FILES}; \
	do \
		rm -f $(TARGET_OUT_STAGING)/usr/sbin/$$f; \
	done
endef

include $(BUILD_AUTOTOOLS)

